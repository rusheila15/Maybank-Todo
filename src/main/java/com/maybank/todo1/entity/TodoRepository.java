package com.maybank.todo1.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Integer> {
	
	List<Todo> findAll(); 
	
}