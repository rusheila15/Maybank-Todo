package com.maybank.todo1.service;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

import com.maybank.todo1.entity.Todo;

public interface TodoService {
	
	List<Todo> listAll();
	Optional <Todo> getTodoById(int id);
    void add(String name, String description, Date targetDate, int starMeter);
    void delete(int id);
    void save(Todo todo);
    void update(Todo todo);
	void add(String name, String description, java.util.Date targetDate, int starMeter);

}
